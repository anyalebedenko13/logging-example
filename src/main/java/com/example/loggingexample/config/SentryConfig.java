package com.example.loggingexample.config;

import io.sentry.Sentry;
import io.sentry.SentryClient;
import io.sentry.spring.SentryExceptionResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;

import java.security.InvalidParameterException;

@Configuration
public class SentryConfig {
    private final Logger logger = LoggerFactory.getLogger(SentryConfig.class);

    @Bean
    public SentryClient backendSentryInitialization(@Value("${sentry.dsn}") String dsn) {
        if (!dsn.isEmpty()) {
            logger.info("Initiated backend Sentry");
            return Sentry.init(dsn);
        } else {
            throw new InvalidParameterException("dsn was empty");
        }
    }

    @Bean
    public HandlerExceptionResolver sentryExceptionResolver() {
        return new SentryExceptionResolver();
    }
}

