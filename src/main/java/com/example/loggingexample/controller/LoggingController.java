package com.example.loggingexample.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoggingController {
   private final Logger logger = LoggerFactory.getLogger(LoggingController.class);

    @GetMapping("/logLevels")
    public void logLevels() {
        logger.info("this is info message");
        logger.debug("this is debug message");
        logger.warn("this is warn message");
        logger.error("this is error message");
    }
}
