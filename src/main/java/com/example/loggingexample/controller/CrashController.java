package com.example.loggingexample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CrashController {

    @GetMapping("/oups/{message}")
    public void triggerException(@PathVariable String message) {
        throw new Error(message);
    }
}
